class CreateEventGuests < ActiveRecord::Migration
  def change
    create_table :event_guests do |t|

      t.timestamps null: false
      t.belongs_to :user, index: true
      t.belongs_to :event, index: true

    end
  end
end
