class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :description
      t.integer :accessibility, default: 0, null: false
      t.integer :capacity
      t.date :day
      t.time :hour

      t.timestamps null: false
    end
  end
end
