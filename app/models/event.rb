class Event < ActiveRecord::Base
  belongs_to :user

  has_many   :event_guests
  has_many   :guests, :through => :event_guests, :source => :user
  validates :user_id, presence: true
  validates :name, presence: true
  validates :description, presence: true

  enum accessibility: ["Public", "Invitation only", "Private"]

  mount_uploader :picture, PictureUploader
end
