class User < ActiveRecord::Base

  has_secure_password
  before_save { self.email = email.downcase }
  MAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :name, presence: true
  validates :email, presence: true, format: { with: MAIL_REGEX }, uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 8 }

  has_many :event_guests, dependent: :destroy
  has_many :events, :through => :event_guests

end
