class SessionsController < ApplicationController
  def new
    if !current_user.nil?
      redirect_to current_user
    end
  end
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to user
    else
      flash.now[:danger] = 'There has been a login error'
      render 'new'
    end
  end

  def destroy
  end

end
