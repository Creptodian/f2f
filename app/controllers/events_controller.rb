class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :add, :invite, :guests]
  before_action :force_login, only: [:index, :new, :invite, :show, :edit, :update, :destroy, :add]

  # GET /events
  # GET /events.json
  def index
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @event = Event.find(params[:id])
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  def invite
  end

  def uninvite(guest)
    @event.guests.delete(guest)
  end

  def add
    respond_to do |format|

      #esto habria que toquetearlo mas para que mostrara lo de los errores
      if @event.capacity.nil? || @event.guests.count < @event.capacity

        if !User.find_by(email: params[:email]).nil?
          @event.guests << User.find_by(email: params[:email])
          format.html { redirect_to @event, notice: 'Event was successfully updated.' }
          format.json { render :show, status: :ok, location: @event }

        else
          format.html { render :show, notice: 'User couldnt be registered'  }
          format.json { render json: @event.errors, status: :unprocessable_entity }
        end
      else

        format.html { render :show, notice: 'This event capacity is full'  }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end

    end
  end

  def guests

  end


  # POST /events
  # POST /events.json
  def create

    @event = Event.new(event_params)
    @event.user_id = current_user.id

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:name, :description, :accessibility, :capacity, :day, :hour, :picture)
  end

  def force_login
    if !logged_in?
      redirect_to '/login'
    end
  end
end
