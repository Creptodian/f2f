// Código adaptado desde https://github.com/wycats/rack-offline
// Almacena y recupera la lista de origamis en el LocalStorage, además de generar
// el html necesario para inyectarlo en el DOM.
// Incluye el almacenamiento de imágenes en el localStore
// No incluye la lógica para mostrar la página de un Origami, ni elimina elementos del
// localStore, etc...



$(document).on('pageinit', function(){

    var date, hour;

    // Almacena en el local storage la imagen cuya url se pasa como parámetro
    function saveImageLocal(key, url){

        var nimg = new Image();
        nimg.setAttribute('crossOrigin', 'anonymous');
        nimg.src =url;
        //habilitarmos crossed origins para que no nos salga alerta de seguridad! :3c
        nimg.onload = function(){
            var canvas = document.createElement('canvas');
            canvas.setAttribute('crossOrigin', 'anonymous');
            var context = canvas.getContext('2d');
            context.canvas.height=nimg.height;
            context.canvas.width=nimg.width;
            context.drawImage(nimg, 0,0,nimg.width, nimg.height);
            var data = canvas.toDataURL("image/png");
            localStorage.setItem(key, data);
        };
    }

    function getTime(data){
        var d = new Date(data.day);
        var h = new Date(data.hour);

        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        date = d.getDate() + "-" + monthNames[d.getMonth()] + "-" + d.getFullYear(); //Months are zero based
        hour = h.getHours() + ":" + h.getMinutes();

    }


    // Construye la lista de origamis en el DOM con los datos json que recibe en 'data'
    function incluye_eventos(data){
        console.log("callback de ajax refresh");
        $('#events').html('');

        for (var event in data) {
            var auxdata = localStorage[data[event].id+".thumb"];
            if (auxdata == null){
                auxdata = "http://res.cloudinary.com/dtpz2dck6/image/upload/c_fill,h_100,w_100/v1459530118/ju0zcntq1rt78ftzfuhv.png"; }

            var capacity = "";

            if (!data[event].capacity == null || data[event].capacity > 0) {
                capacity = data[event].capacity + " | ";
            }

            getTime(data[event]);
            $('#events').append("<div class='event'><a href='/events/" + data[event].id + "'>" +
                "<img class='event-thumb' src='" + auxdata + "' /></a>" +
                "<div class='info'><h1><a href='/events/" + data[event].id +"'>"+ data[event].name +"</a></h1>" +
                "<u> " + data[event].accessibility + "</u> | " + capacity + date + " " + hour + "</div></div>");

        }


    }


    function incluye_eventos_show(data){
        for (var event in data) {
            var capacity = "";

            if (!data[event].capacity == null || data[event].capacity > 0) {
                capacity = data[event].capacity + " | ";
            }
            $('#name-' + data[event].id).html('');
            $('#name-' + data[event].id).append(data[event].name);

            var auxdata = localStorage[data[event].id];
            if (auxdata == null){ auxdata = "http://res.cloudinary.com/dtpz2dck6/image/upload/v1459530118/ju0zcntq1rt78ftzfuhv.png"; }

            $('#event-' + data[event].id).html('');
            $('#event-'+ data[event].id).append("<img id ='img-event' src='" + auxdata + "' /><p>" +
                "<b>Description:</b> " + data[event].description + "</p><p><u>" + data[event].accessibility + "</u> | " +
                capacity + date + " " + hour + "</p>");
        }
    }

    //  Declaramos una función que recibe un objeto js,
    //  lo almacena en el local storage y actualiza el html
    var actualizaEventos = function(object) {
        localStorage.events = JSON.stringify(object);
        for (var event in object) {
            if (object[event].picture.picture != null) {
                saveImageLocal(object[event].id + ".thumb", object[event].picture.picture.thumb.url);
                saveImageLocal(object[event].id, object[event].picture.picture.url);
            }


        }
        $("#events").html(incluye_eventos(object));
        $("#eventid").html(incluye_eventos_show(object));


    };


    // flag para no machacar al servidor si ya está actualizando
    var updating = false;

    // función para actualizar origamis desde el servidor
    var actualizaDesdeServidor = function() {
        // no machacamos al servidor si ya estamos en ello
        if(updating) return;

        updating = true;

        $.mobile.loading( 'show');
        $.getJSON("/events.json?"+window.location.search.substring(1)).done( function(json) {
            console.log("url search: " + window.location.search.substring(1));
            actualizaEventos(json);
        }).fail(function() {
            console.log( "error al acceder al servidor" );
        }).always(function() {
            $.mobile.loading( 'hide');
            updating = false;
            console.log( "complete" );
        });
    };

    // Actualizarmos el html si tenemos origamis en el localStorage.
    // Si no estamos 'online', al menos se mostrará lo que tenemos en el storage

    if(localStorage.events) actualizaEventos(JSON.parse(localStorage.events));

    // si el usuario entra 'online' desde 'offline'
    // accedemos al servidor para atualizar los origamis
    $(window).on("online", actualizaDesdeServidor);

    // Si el usuario está online. Actualizar la lista ahora
    if(window.navigator.onLine) actualizaDesdeServidor();
});